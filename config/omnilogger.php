<?php

return [
    'url' => [
        'aws_local' => [
            'logapp' => env('LOGAPP_AWS_LOCAL_URL', 'http://log-app.test'),
        ],
    ],
];
