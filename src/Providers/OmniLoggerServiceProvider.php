<?php

namespace Superius\OmniLogger\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;
use Superius\OmniLogger\Services\CustomLogger;
use Superius\OmniLogger\Services\CustomLogHandler;

class OmniLoggerServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        config([
            'logging.channels.custom' => [
                'driver' => 'custom',
                'handler' => CustomLogHandler::class,
                'via' => CustomLogger::class,
            ],
        ]);
    }
    public function boot(): void
    {
        if ($this->app->environment('local')) {
            $source = base_path('vendor/superius/omni-logger/config/omnilogger.php');
            $destination = base_path('config/omnilogger.php');

            File::copy($source, $destination);
        }
    }

}
