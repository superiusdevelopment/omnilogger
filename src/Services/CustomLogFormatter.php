<?php

namespace Superius\OmniLogger\Services;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Monolog\Formatter\NormalizerFormatter;
use Monolog\LogRecord;

class CustomLogFormatter extends NormalizerFormatter
{
    /**
     * @param \Monolog\LogRecord $record
     * @return array<string,mixed>
     */
    public function format(LogRecord $record): array
    {
        $record = parent::format($record);

        return $this->getRequestData($record);
    }

    /**
     * Convert a log message into an MariaDB Log entity
     *
     * @param array<string,mixed> $record
     * @return array<string,mixed>
     */
    protected function getRequestData(array $record): array
    {
        $data = [];
        $data['mid'] = self::getMarketContext();
        $data['type'] = Str::lower(data_get($record, 'level_name'));
        $data['level'] = Str::lower(data_get($record, 'context.level', 'level_1'));
        $data['message'] = data_get($record, 'message');
        $data['app_name'] = config('app.name');
        $data['method'] = data_get($record, 'context.method');
        $data['log_time'] = Carbon::parse(data_get($record, 'datetime'))->format('Y-m-d H:i:s');

        return $data;
    }

    private static function getMarketContext(): ?int
    {
        $classesToCheck = [
            'Superius\OmniApp\Helpers\MarketContext',
            'Superius\OmniHub\Helpers\MarketContext',
        ];

        foreach ($classesToCheck as $class) {
            if (class_exists($class)) {
                $instance = app($class);
                if (method_exists($instance, 'getMarket')) {
                    return $instance->getMarket(false);
                } elseif (method_exists($instance, 'getMarketSlug')) {
                    return $instance->getInstance()->getMarketId(false);
                }
            }
        }

        return null;
    }
}
