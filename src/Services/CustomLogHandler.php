<?php

namespace Superius\OmniLogger\Services;

use Illuminate\Support\Facades\Log;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\LogRecord;
use Superius\OmniAppRouter\Requests\LocalHttpRequest;

class CustomLogHandler extends AbstractProcessingHandler
{
    protected function write(LogRecord $record): void
    {
        //send request to the log app
        $response = (new LocalHttpRequest(config('omnilogger.url.aws_local.logapp')))
            ->post('/api/a2a/logs', $record->formatted);

        if ($response->status() !== 200) {
            Log::channel('stack')->error('Cannot send log data to the LogApp. Status code: ' .
                $response->status() . '. Body: ' . $response->body());

            Log::channel('stack')->error(print_r($record->formatted, true));
        }
    }

    protected function getDefaultFormatter(): CustomLogFormatter
    {
        return new CustomLogFormatter();
    }
}
