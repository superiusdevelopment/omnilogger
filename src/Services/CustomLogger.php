<?php

namespace Superius\OmniLogger\Services;

use Monolog\Logger;

class CustomLogger
{
    /**
     * Create a custom Monolog instance.
     */
    public function __invoke(array $config): Logger
    {
        $logger = new Logger('custom');
        $logger->pushHandler(new CustomLogHandler());

        return $logger;
    }
}
